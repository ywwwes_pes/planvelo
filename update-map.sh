#!/bin/bash

URL=https://cocarto.com/en/layers/169fb49d-f0d4-440d-bb46-3f5308881f6e.geojson?authkey=Pfoq9bpvPpkjY9j5

# pull new geojson from gis
# requires jq for formatting https://stedolan.github.io/jq/
curl -L $URL  | jq > map.json
