FROM ruby:2.4
COPY Gemfile /tmp
WORKDIR /tmp
RUN bundle install
WORKDIR /srv
ENV JEKYLL_ENV test
CMD bundle exec jekyll serve --config _config.yml,_config_stage.yml --incremental -H 0.0.0.0
