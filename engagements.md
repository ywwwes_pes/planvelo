---
layout: page
title: Engagements du Plan vélo
short_title: Engagements
description: Les engagements pris par la mairie de Paris pour le Plan Vélo 2021-2026
permalink: /engagements/
image: assets/images/Plan_Velo_2021_2026_small.jpg
toc: false
---


Le précédent [Plan Vélo 2015-2020, dont nous avions suivi la réalisation à travers notre Observatoire](https://observatoire.parisenselle.fr/2020), avait atteint 55 % du linéaire promis en 2015. Malgré ce résultat très en deçà des objectifs annoncés, ce premier Plan Vélo a marqué un tournant historique pour le vélo et permis une forte augmentation du nombre d’usagers et d’usagères du vélo du quotidien à Paris.

La création de grands axes cyclables comme la rue de Rivoli ou le boulevard de Sébastopol en 2019 ont permis une forte augmentation du nombre d’usagers du vélo. Mais cette période paraît déjà lointaine face à l’explosion récente de la pratique du vélo permise par une combinaison d'événements : d’abord la grève des transports à l’hiver 2019/2020, qui a converti de nombreux Franciliens au vélo, mais surtout les 52 km de coronapistes déployées en 2020 en réponse à la crise sanitaire. Résultat : entre 2019 et 2021, la fréquentation des pistes cyclables a augmenté de 78%.



## Le Plan Vélo 2021-2026

Le 21 octobre 2021, David Belliard, adjoint à la Maire de Paris en charge de la transformation de l’espace public, des transports, des mobilités, du code de la rue et de la voirie, [a annoncé le nouveau plan vélo 2021-2026](https://twitter.com/David_Belliard/status/1451130657917243395).

Doté d’un budget de 250 millions d’euros, son objectif est de faire de Paris une ville 100 % cyclable en déployant un total de 180 km d’aménagements cyclables d’ici la fin de la mandature : 130&nbsp;km de nouvelles pistes cyclables et la pérennisation d’ici 2024 des 52&nbsp;km de coronapistes.

### Le réseau cyclable : 180&nbsp;km de pistes supplémentaires

Au total, 180&nbsp;km de pistes cyclables viendront compléter le réseau existant : 
* 130&nbsp;km de nouvelles pistes
* 52&nbsp;km de coronapistes pérennisées

Pour ce qui est de la qualité de réalisation de ces pistes, deux niveaux de réseau existent :
Le réseau principal - ou Vélopolitain - est le réseau cyclable structurant, à haut niveau de service, et intègre les axes du futur réseau régional, le RER Vélo. Il s’insère également dans le réseau vélo de la Métropole du Grand Paris. Les infrastructures cyclables du réseau principal suivent les recommandations du CEREMA : les pistes sont sécurisées, séparées du trafic, larges et confortables.
Le réseau secondaire complète plus finement le maillage du réseau structurant. La ville de Paris ne s’est pas engagée à suivre de cahier des charges qualitatif pour le réaliser.

<br>

![]({{ "/assets/images/Plan_Velo_2021_2026.jpg" | relative_url }})

<br>

**Priorité à la pérennisation.** En juillet 2021, [David Belliard annonçait que la totalité des coronapistes serait pérennisée](https://www.leparisien.fr/info-paris-ile-de-france-oise/transports/plan-velo-a-paris-nous-allons-investir-250-millions-annonce-la-ville-21-10-2021-FRIQYNSGMFCSPOJSEXZ2ZLLSRA.php) avant les Jeux Olympiques et Paralympiques de Paris 2024. Voici le calendrier des travaux communiqué dans les pages du journal “20 Minutes” (©Statista/20 minutes). Le calendrier au-delà des JO n’est pas connu.

<br>

![]({{ "/assets/images/calendier_perennisations.png" | relative_url }})

<br>

**Généralisation des doubles-sens cyclables.** Le Plan Vélo annonce la généralisation de ces doubles-sens cyclables dans toutes les rues de Paris, avec un objectif chiffré : 390 km d’ici la fin du mandat.

Le code de la route prévoit que toutes les zones 30 sont à double-sens cyclable. Dans le cadre de la mise à 30 km/h de Paris, la mairie va donc systématiser les double-sens cyclables (DSC). Raccourcis sûrs quoique pas toujours confortables ou rassurants pour les usagers du vélo, les DSC doivent s’accompagner de réelles politiques de modération de la vitesse et de la pression automobile pour constituer des itinéraires utiles.

**Sécurisation des carrefours et des portes de Paris.** Les carrefours et les portes de Paris sont aujourd’hui de véritables points noirs pour les usagers. Alors qu’aux Pays-Bas tous les carrefours sont aménagés pour permettre à celles et ceux qui se déplacent à vélo d’y circuler en sécurité, à Paris, les carrefours sont trop souvent difficilement franchissables à vélo, anxiogènes et dangereux. La ville de Paris s’engage à résorber ces coupures urbaines dans le Plan Vélo.

### Stationnement vélo : 130&nbsp;000 places pour répondre à tous les besoins

Le stationnement vélo est une attente forte des Parisiens et Parisiennes, pour répondre à différents types de besoin : stationnement résidentiel, au bureau/à la fac, en déplacement du quotidien (commerces, infrastructures).

Le Plan Vélo 2021-2026 prévoit le déploiement de plus de 130&nbsp;000 nouvelles places de stationnement :

* Stationnement libre service

  30&nbsp;000 nouvelles places en arceaux sur l’espace public, dont 1&nbsp;000 places pour les vélo cargos (soit 15 000 arceaux au total)

* Stationnement vélo sécurisé
  100&nbsp;000 places de stationnement vélo sécurisé au total dont 
   - 10&nbsp;000 sur l’espace public ou en parking
   - 40&nbsp;000 à proximité des gares
   - 50&nbsp;000 dans le privé

### Rendre les déplacements à vélo sûrs et confortables

Pour permettre à toutes celles et ceux qui le souhaitent de se déplacer à vélo, il est indispensable de rendre sa pratique sûre et confortable. Avec cet objectif en tête, la ville de Paris s’engage donc à :

* développer les carrefours à la hollandaise
* mettre en place un jalonnement spécifique pour les cyclistes
* renforcer le nettoyage des pistes cyclables
* mobiliser la police municipale nouvellement créée pour faire respecter la priorité piétons et les pistes cyclables (stationnement illicite)

### Culture vélo

* Généralisation du Savoir Rouler à l’école, pour apprendre aux jeunes Parisiens et Parisiennes à se déplacer à vélo en toute sécurité
* Aides à l’écomobilité (aide à l’achat d’un vélo à assistance électrique ou d’un vélo mécanique, récemment réformée)
* Développement de la cyclologistique, etc.

### La ville de Paris veut montrer l’exemple

La ville de Paris se fixe des objectifs en tant qu’employeur :

* objectif 20 % des déplacements professionnels à vélo à la fin du mandat
* mise en place du Forfait Mobilités Durables (FMD)
* création de stationnements vélos sécurisés sur tous les sites administratifs de la ville
* intégration des dispositifs anti angles-morts dans les commandes publiques de la Ville.


### Le budget

![]({{ "/assets/images/plan_velo_budget.png" | relative_url }})

### Gouvernance

Le Plan Vélo est principalement piloté par la Mission Vélo (anciennement Mission Aménagements Cyclables), section de la Direction de la Voirie et des Déplacements, sous la responsabilité de David Belliard, adjoint à la Maire de Paris en charge de la transformation de l’espace public, des transports, des mobilités, du code de la rue et de la voirie. Créée suite aux demandes pressantes de Paris en Selle en 2018 et longtemps dotée d’une équipe restreinte, la Mission Vélo compte désormais 17 personnes qui travaillent à temps plein à la réalisation du Plan Vélo de la Ville.



